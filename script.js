//Functions
const callAdd = (id, name, desc, price, active) =>
{
	let obj = {
		id: id,
		name: name,
		description: desc,
		price: price,
		isActive: active
	}

	courses.push(obj)
	alert(`You have created ${name}. Its price is ${price}.`)
} 

const getSingleCourse = (id) => courses.find(obj => obj.id === id)
		
const getAllCourses = () => console.log(courses);

const archiveCourse = (index) => {
	if(courses[index].isActive === false){
		console.log(`This is already inactive.`)
	} else {
		courses[index].isActive = false;
		console.log(courses[index]);
	}
};

const deleteCourse = () => courses.pop();


//Stretch Goals
const getActive = () => courses.filter(obj => obj.isActive === true) 

const archiveCourseStretch = (name) => {
	let index = courses.findIndex(obj => obj.name === name);
	if(courses[index].isActive === false){
		console.log(`This is already inactive.`)
	} else {
		courses[index].isActive = false;
		console.log(courses[index]);
	}
};



//Variables
let courses = [
	{
		id: "MEC102",
		name: "Mechanical Bodies",
		description: "Using static and dynamic formula",
		price: 2500,
		isActive: true
	},
	{
		id: "ENG103",
		name: "English Workplace",
		description: "Techincal interviews tips",
		price: 1200,
		isActive: false
	},
	{
		id: "IT114",
		name: "User Interface",
		description: "Creating graphic user interface",
		price: 600,
		isActive: true
	},
	{
		id: "PHY022",
		name: "Physics Dynamics",
		description: "Learning gravity and height formula",
		price: 3800,
		isActive: true
	}
];


//Try this
/*

callAdd("MATH160","Integral Calculus","This is so hard.",1120,true);
getSingleCourse("MEC102");
getAllCourses();
deleteCourse();
archiveCourse(1);




Stretch Goals:
getActive();
archiveCourseStretch("Mechanical Bodies");
*/